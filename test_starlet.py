import starlet

import unittest

# python test_starlet.py

def create_dict(keys, avalue):
	mydict = {}
	original = mydict
	for key in keys[:-1]:
		mydict[key] = {}
		mydict = mydict[key]
	mydict[keys[-1]] = avalue
	return original

def update_dict(dict, keys, avalue):
	original = dict
	mydict = dict
	for key in keys[:-1]:
		if key not in mydict:
			mydict[key] = {}
		mydict = mydict[key]
	mydict[keys[-1]] = avalue
	return original


class TestStarletLambda(unittest.TestCase):

	def testClean(self):
		self.assertEqual("Frank", starlet.clean("Frank's"))
		self.assertEqual("Frank", starlet.clean("Frank"))
		self.assertEqual("Lara", starlet.clean("Lara's"))
		self.assertEqual("Lara", starlet.clean("Lara"))
		self.assertEqual("Mummy", starlet.clean("Mummy"))
		self.assertEqual("Mummy", starlet.clean("Mummys"))
		self.assertEqual("Daddie", starlet.clean("Daddies"))


	def testExtractData(self):
		test_data = create_dict(["slots","Child","value"], "Frank")
		test_data = update_dict(test_data, ["slots","ToAdd","value"], 22)
		self.assertEqual("Frank", starlet.extract_data(test_data)['child'])
		self.assertEqual(22, starlet.extract_data(test_data)['add'])

	def testAwaitingPassword(self):
		test_session = create_dict(["attributes","awaiting_password"], True)
		self.assertEqual(True, starlet.is_awaiting_password(test_session))

	def testCreateDict(self):
		mydict = create_dict(["level1"], 10)
		self.assertEqual({"level1": 10}, mydict)

if __name__ == '__main__':
	unittest.main()

