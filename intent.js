{
  "intents": [
    {
      "intent": "ChildStarScoreQuery",
      "slots": [
        {
          "name": "Child",
          "type": "CHILD"
        }
      ]
    },
    {
      "intent": "ChildStarScoreAdd",
      "slots": [
        {
          "name": "Child",
          "type": "CHILD"
        },
        {
          "name": "ToAdd",
          "type": "AMAZON.NUMBER"
        }
      ]
    },
    {
      "intent": "ChildStarScoreRemove",
      "slots": [
        {
          "name": "Child",
          "type": "CHILD"
        },
        {
          "name": "ToRemove",
          "type": "AMAZON.NUMBER"
        }
      ]
    },
    {
      "intent": "ChildStarScoreUpdatePassword",
      "slots": [
        {
          "name": "Password",
          "type": "LETTER_OR_NUMBER"
        }
      ]
    },
   {
      "intent": "AMAZON.CancelIntent"
    },
    {
      "intent": "AMAZON.StopIntent"
    },
    {
      "intent": "AMAZON.HelpIntent"
    }
  ]
}