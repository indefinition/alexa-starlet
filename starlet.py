"""
This sample demonstrates a simple skill built with the Amazon Alexa Skills Kit.
The Intent Schema, Custom Slots, and Sample Utterances for this skill, as well
as testing instructions are located at http://amzn.to/1LzFrj6

For additional samples, visit the Alexa Skills Kit Getting Started guide at
http://amzn.to/1LGWsLG
"""

from __future__ import print_function

import boto3
import json
import datetime
from random import randint


# --------------- Helpers that build all of the responses ----------------------

def build_speechlet_response(title, output, reprompt_text, should_end_session):
    return {
        'outputSpeech': {
            'type': 'SSML',
            'ssml': "<speak>" + output + "</speak>"
        },
        'card': {
            'type': 'Simple',
            'title': "Starlet - " + title,
            'content': "Starlet - " + output
        },
        'reprompt': {
            'outputSpeech': {
                'type': 'SSML',
                'ssml': "<speak>" + str(reprompt_text) + "</speak>"
            }
        },
        'shouldEndSession': should_end_session
    }


def build_response(session_attributes, speechlet_response):
    return {
        'version': '1.0',
        'sessionAttributes': session_attributes,
        'response': speechlet_response
    }

# --------------- Functions that control the skill's behavior ------------------

def get_problem_response():
    session_attributes = {}
    card_title = "What did you say?"
    speech_output = "Ask for a score or to add or remove stars."

    # If the user either does not reply to the welcome message or says something
    # that is not understood, they will be prompted again with this text.
    reprompt_text = "Please try again. Would you like to know a score, or add or remove stars?"
    should_end_session = False
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def get_welcome_response():
    """ If we wanted to initialize the session to have some attributes we could
    add those here
    """

    session_attributes = {}
    card_title = "Welcome"
    speech_output = "Ask Starlet for a star score. Or to add or remove stars from a child's score."

    # If the user either does not reply to the welcome message or says something
    # that is not understood, they will be prompted again with this text.
    reprompt_text = "Ask Starlet for a star score, or to add or remove."
    should_end_session = False
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def get_help_response():
    session_attributes = {}
    card_title = "Some Help"
    names = get_all_names()
    names_list = ",".join(names[:-1]) + " and " + names[-1]
    speech_output = "Ask for a score, or to add or remove stars. I am tracking scores for " + names_list

    # If the user either does not reply to the welcome message or says something
    # that is not understood, they will be prompted again with this text.
    reprompt_text = "Ask Starlet for a star score, or to add or remove."
    should_end_session = False
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def handle_session_end_request():
    card_title = "Session Ended"
    speech_output = "Bye eee! Have a lovely day!"
    # Setting this to true ends the session and exits the skill.
    should_end_session = True
    return build_response({}, build_speechlet_response(
        card_title, speech_output, "", should_end_session))

def get_item_from_dynamodb(table, primkey_field, primkey_val):
    client = boto3.resource('dynamodb')
    table = client.Table(table)
    dbitem = table.get_item(Key={primkey_field:primkey_val})
    return dbitem

def get_value_from_dynamodb(table, primkey_field, primkey_val, field):
    dbitem = get_item_from_dynamodb(table, primkey_field, primkey_val)
    return dbitem['Item'][field]

def get_all_values_from_dynamodb(table):
    client = boto3.resource('dynamodb')
    table = client.Table(table)
    items = table.scan()
    return items

def get_names_and_scores():
    children = get_all_values_from_dynamodb("StarScore")
    res = ""
    for child in children['Items']:
        print(child)
        res = res + child['name'] + " has " + str(child['score']) + " stars. "
    return res

def get_child_data(name):
    item = get_item_from_dynamodb("StarScore", "name", name)
    if not item and name[:-1] == "s":
        item = get_item_from_dynamodb("StarScore", "name", name[0:-1])
    return item

def get_all_names():
    items = get_all_values_from_dynamodb("StarScore")
    names= []
    for item in items['Items']:
        names.append(item['name'])
    return names

def update_value_in_dynamodb(table, primkey_field, primkey_val, field, field_val):
    client = boto3.resource('dynamodb')
    table = client.Table(table)
    table.update_item(
    Key={primkey_field:primkey_val},
    UpdateExpression='SET ' + field + ' = :val1',
    ExpressionAttributeValues={
        ':val1': field_val
    }
)

def get_child_id_from_db(child):
    print("Looking up " + child)
    child_low = child.lower()
    items = get_all_values_from_dynamodb("StarScore")
    for item in items['Items']:
        name_low = item['name'].lower()
        if name_low == child_low or name_low+"s" == child_low:
            return item['child_id']
    print("Couldn't find " + child)
    raise LookupError("Child not found: " + child)

def clean(child):
    if "'s" in child:
        return child[:-2]
    elif child[-1] == "s":
        return child[:-1]
    else: 
        return child

def send_update(child, change, new_score):
    child_upper = child[0].upper() + child[1:]
    send_message(child_upper + "'s score has been updated by " + str(change) + " and is now " + str(new_score) + " stars.")

def send_warning(child):
    child_upper = child[0].upper() + child[1:]
    send_message("Warning - an attempt to change " + child_upper + " score by more than 20 has been made.")

def send_message(msg):
    client = boto3.client('sns')
    response = client.publish(
        TargetArn="arn:aws:sns:eu-west-1:072196213258:AlexaStarletNotification",
        Message=msg,
        MessageStructure='string')    

def query_star_score(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = False

    data = extract_data(intent)
    if 'child' not in data:
        speech_output = get_names_and_scores()
        reprompt_text = ""
    elif 'child' in data:
        child = data['child']
        clean_name = clean(child)
        session_attributes['awaiting_password'] = False
        try:
            print("User asking for child " + clean_name)        
            score = str(get_value_from_dynamodb('StarScore', 'child_id', get_child_id_from_db(clean_name), 'score'))
            star_str = " star" if int(score) == 1 else " stars"
            speech_output = clean_name + " has " + score + star_str
            reprompt_text = ""
        except:
            speech_output = "Could not find a record for " + child
            reprompt_text = "Please ask again."
    else:
        print("Bad slot!")
        speech_output = "Sorry I didn't get that"
        reprompt_text = ""
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def prepare_add_star_score(intent, session):
    card_title = "Starlet adding stars"
    session_attributes = {}
    should_end_session = False

    data = extract_data(intent)
    if 'child' in data and 'add' in data:
        try:
            addstr = data['add']
            addme = int(addstr)
            child = data['child']
            clean_name = clean(child)
            if addme > 20:
                send_warning(clean_name)
                speech_output = "I'm afraid the maximum you can award is 20 stars."
                reprompt_text = "You can ask to add less than 20."
            else:
                try:
                    child_id = get_child_id_from_db(clean_name)
                    current_value = int(get_value_from_dynamodb('StarScore', 'child_id', child_id, 'score'))
                    new_score = (current_value + addme)     
                    rand_letter = randint(1, min(7, len(get_todays_password())))
                    speech_output = "I will update " + clean_name + "'s score to " + str(new_score) + " if you tell me letter " + str(rand_letter) + " from the password"
                    session_attributes['rand_letter'] = rand_letter
                    session_attributes['child'] = clean_name
                    session_attributes['delta'] = addme   
                    session_attributes['score'] = new_score
                    session_attributes['child_id'] = child_id
                    session_attributes['awaiting_password'] = True
                    reprompt_text = "Please tell me letter " + str(rand_letter) + " from the password."
                except:
                    speech_output = "Could not find a record for " + child
                    reprompt_text = "Please ask again."  
        except:
            print("Problem adding: " + str(intent) + " : " + str(data))
            speech_output = "Sorry, please try again."
            reprompt_text = speech_output

        child = data['child']            
        clean_name = clean(child)              

    else:
            print("Problem adding: add: " + str(intent))
            speech_output = "Sorry, please try again."
            reprompt_text = speech_output
    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def prepare_remove_star_score(intent, session):
    card_title = "Starlet removing stars"
    session_attributes = {}
    should_end_session = False

    data = extract_data(intent)
    if 'child' in data and 'remove' in data:
        to_remove = int(data['remove'])
        child = data['child']
        clean_name = clean(child)

        if to_remove > 20:
            send_warning(clean_name)
            speech_output = "I'm afraid the maximum you can remove is 20 stars."
            reprompt_text = "Please ask to remove less than 20."
        else:
            try:
                child_id = get_child_id_from_db(clean_name)
                current_value = int(get_value_from_dynamodb('StarScore', 'child_id', child_id, 'score'))
                new_score = max((current_value - to_remove), 0)
                rand_letter = randint(1, min(7, len(get_todays_password())))  
                speech_output = "I will update " + clean_name + "'s score to " + str(new_score) + " if you tell me letter " + str(rand_letter) + " from the password"
                session_attributes['rand_letter'] = rand_letter
                session_attributes['child'] = clean_name
                session_attributes['delta'] = to_remove * -1
                session_attributes['score'] = new_score
                session_attributes['child_id'] = child_id
                session_attributes['awaiting_password'] = True
                reprompt_text = "Please tell me letter " + str(rand_letter) + " from the password."
            except:
                speech_output = "Could not find a record for " + child
                reprompt_text = "Please ask again."

    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def password_and_update_star_score(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = False

    if session['attributes']['awaiting_password']:
        data = extract_data(intent)
        if 'password' in data:
            password = data['password']
            print("Password is " + password)
            rand_letter = session['attributes']['rand_letter']
            print("Rand letter is " + str(rand_letter))
            if password.lower() == get_todays_password().lower()[rand_letter-1]:
                child = session['attributes']['child']
                new_score = session['attributes']['score']
                delta = session['attributes']['delta']
                child_id = session['attributes']['child_id']
                update_value_in_dynamodb('StarScore', 'child_id', child_id, 'score', new_score)
                send_update(child, delta, new_score)
                speech_output = "I've updated the score to " + str(new_score) + ". Thanks"
                session_attributes['awaiting_password'] = False
            else:
                speech_output = "I'm sorry, the password is not correct, please try again"        
            reprompt_text = ""
    else:
        speech_output = "I don't need a password yet."
    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))    

def extract_data(intent):
    intent_data = {}
    if 'Child' in intent['slots'] and 'value' in intent['slots']['Child']:
        intent_data['child'] = intent['slots']['Child']['value']
    if 'ToAdd' in intent['slots'] and 'value' in intent['slots']['ToAdd']:
        intent_data['add'] = intent['slots']['ToAdd']['value']
    if 'ToRemove' in intent['slots'] and 'value' in intent['slots']['ToRemove']:
        intent_data['remove'] = intent['slots']['ToRemove']['value']
    if 'Password' in intent['slots'] and 'value' in intent['slots']['Password']:
        intent_data['password'] = intent['slots']['Password']['value']
    if 'AddRemove' in intent['slots'] and 'value' in intent['slots']['AddRemove']:
        intent_data['add_remove'] = intent['slots']['AddRemove']['value']
    if 'Amount' in intent['slots'] and 'value' in intent['slots']['Amount']:
        intent_data['amount'] = intent['slots']['Amount']['value']
    return intent_data

# Get Day+Month concatenated, e.g. MondayJanuary
def get_todays_password():
    return datetime.datetime.now().strftime("%A") + datetime.datetime.now().strftime("%B")

# --------------- Events ------------------

def on_session_started(session_started_request, session):
    """ Called when the session starts """

    print("on_session_started requestId=" + session_started_request['requestId']
          + ", sessionId=" + session['sessionId'])


def on_launch(launch_request, session):
    """ Called when the user launches the skill without specifying what they
    want
    """

    print("on_launch requestId=" + launch_request['requestId'] +
          ", sessionId=" + session['sessionId'])
    # Dispatch to your skill's launch
    return get_welcome_response()


def on_intent(intent_request, session):
    """ Called when the user specifies an intent for this skill """

    print("on_intent requestId=" + intent_request['requestId'] +
          ", sessionId=" + session['sessionId'])

    intent = intent_request['intent']
    intent_name = intent_request['intent']['name']

    awaiting_password = is_awaiting_password(session)
    print("Awaiting password: " + str(awaiting_password))

    # Dispatch to your skill's intent handlers
    if intent_name == "ChildStarScoreQuery":
        return query_star_score(intent, session)
    if intent_name == "ChildStarScoreAdd":
        return prepare_add_star_score(intent, session)
    if intent_name == "ChildStarScoreRemove":
        return prepare_remove_star_score(intent, session)
    if intent_name == "ChildStarScoreUpdatePassword" and awaiting_password:
        return password_and_update_star_score(intent, session)
    if intent_name == "ChildStarScoreUpdatePasswordOneCommand":
        return update_score_with_password(intent, session)
    elif intent_name == "AMAZON.HelpIntent":
        return get_help_response()
    elif intent_name == "AMAZON.CancelIntent" or intent_name == "AMAZON.StopIntent":
        return handle_session_end_request()
    else:
        return get_problem_response()

def is_awaiting_password(session):
    if 'attributes' in session and 'awaiting_password' in session['attributes']:
        return session['attributes']['awaiting_password']
    else:
        return False

def on_session_ended(session_ended_request, session):
    """ Called when the user ends the session.

    Is not called when the skill returns should_end_session=true
    """
    print("on_session_ended requestId=" + session_ended_request['requestId'] +
          ", sessionId=" + session['sessionId'])
    # add cleanup logic here


# --------------- Main handler ------------------

def lambda_handler(event, context):
    """ Route the incoming request based on type (LaunchRequest, IntentRequest,
    etc.) The JSON body of the request is provided in the event parameter.
    """
    print("event.session.application.applicationId=" +
          event['session']['application']['applicationId'])

    """
    Uncomment this if statement and populate with your skill's application ID to
    prevent someone else from configuring a skill that sends requests to this
    function.
    """
    # if (event['session']['application']['applicationId'] !=
    #         "amzn1.echo-sdk-ams.app.[unique-value-here]"):
    #     raise ValueError("Invalid Application ID")

    if event['session']['new']:
        on_session_started({'requestId': event['request']['requestId']},
                           event['session'])

    if event['request']['type'] == "LaunchRequest":
        return on_launch(event['request'], event['session'])
    elif event['request']['type'] == "IntentRequest":
        return on_intent(event['request'], event['session'])
    elif event['request']['type'] == "SessionEndedRequest":
        return on_session_ended(event['request'], event['session'])
